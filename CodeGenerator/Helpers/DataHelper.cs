﻿using System;

namespace CodeGenerator.Helpers
{
    public static class DataHelper
    {
        public static string GetCSharpType(string sqlType)
        {
            switch(sqlType)
            {
                case "date":
                    return "DateTime";
                case "nvarchar":
                    return "string";
                case "decimal":
                    return "decimal";
                case "int":
                    return "int";

                default:
                    throw new Exception("Unknown type encountered.");
            }
        }

        public static string FirstCharToUpper(string s)
        {
            return char.ToUpper(s[0]) + s.Substring(1);
        }

    }


}
