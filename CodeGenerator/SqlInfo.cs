﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace CodeGenerator
{
    public class SqlInfo
    {
        /// <summary>
        /// Gets the SQL table information that is used to build Models and CRUD files
        /// </summary>
        public List<Table> GetTableList(List<string> ignoreTablesList)
        {
            var tableList = new List<Table>();
            // Loop over db tables that are not in the ignoreTablesList 
            foreach (var tableName in GetTableNameList().Where(a => !ignoreTablesList.Any(b => a.EndsWith(b))))
            {
                var table = new Table()
                {
                    Columns = GetColumns(tableName),
                    PrimaryKey = GetTablePrimaryKeyColumnName(tableName),
                    TableName = tableName
                };
                tableList.Add(table);
            }
            return tableList;
        }

        private List<string> GetTableNameList()
        {
            using (var conn = new SqlConnection(Program.connstr))
            {
                return conn.Query<string>("SELECT TABLE_NAME FROM information_schema.tables;").ToList();
            }
        }

        private List<Column> GetColumns(string tableName)
        {
            var sql = "SELECT COLUMN_NAME AS ColumnName, ORDINAL_POSITION AS ColumnId, DATA_TYPE AS DataType, ";
            sql += "CHARACTER_MAXIMUM_LENGTH AS CharacterMaxLength, NUMERIC_PRECISION AS NumericPrecision, ";
            sql += "NUMERIC_SCALE AS NumericScale, prop.value AS [DESCRIPTION], ";
            sql += "CASE col.IS_NULLABLE ";
            sql += "  WHEN 'Yes' THEN 1 ";
            sql += "  WHEN 'No' THEN 0 ";
            sql += "END ";
            sql += "AS IsNullable ";
            sql += "FROM INFORMATION_SCHEMA.TABLES AS tbl ";
            sql += "INNER JOIN INFORMATION_SCHEMA.COLUMNS AS col ON col.TABLE_NAME = tbl.TABLE_NAME ";
            sql += "INNER JOIN sys.columns AS sc ON sc.object_id = object_id(tbl.table_schema + '.' + tbl.table_name) ";
            sql += "AND sc.NAME = col.COLUMN_NAME ";
            sql += "LEFT JOIN sys.extended_properties prop ON prop.major_id = sc.object_id ";
            sql += "AND prop.minor_id = sc.column_id ";
            sql += "AND prop.NAME = 'MS_Description' ";
            sql += $"WHERE tbl.TABLE_NAME = '{ tableName }';";
            using (var conn = new SqlConnection(Program.connstr))
            {
                return conn.Query<Column>(sql).ToList();
            }
        }

        private string GetTablePrimaryKeyColumnName(string tableName)
        {
            var sql = "SELECT Col.Column_Name FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col ";
            sql += "WHERE Col.Constraint_Name = Tab.Constraint_Name AND Col.Table_Name = Tab.Table_Name AND Constraint_Type = 'PRIMARY KEY' ";
            sql += $"AND Col.Table_Name = '{ tableName }' ";
            using (var conn = new SqlConnection(Program.connstr))
            {
                return conn.Query<string>(sql).FirstOrDefault();
            }
        }
       
        public class Column
        {
            public int ColumnId { get; set; }  
            public string ColumnName { get; set; }
            public string Description { get; set; }
            public string DataType { get; set; }
            public int CharacterMaxLength { get; set; }
            public int NumericPrecision { get; set; }
            public int NumericScale { get; set; }
            public bool IsNullable { get; set; }
        }

        public class Table
        {
            public string TableName { get; set; }
            public string PrimaryKey { get; set; } 
            public List<Column> Columns { get; set; }
        }
    }
}
