﻿using CodeGenerator.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static CodeGenerator.SqlInfo;

namespace CodeGenerator
{
    public class ModelGenerator
    {
        public void GenerateModels(List<Table> tableList)
        {
            ///////////////////////////////////////////////////////
            // Generates the model files for each database table
            ///////////////////////////////////////////////////////
            foreach (var table in tableList)
            {
                var sb = new StringBuilder();
                sb.AppendLine("using System;");
                sb.AppendLine("using System.ComponentModel.DataAnnotations;");
                sb.AppendLine("using System.ComponentModel.DataAnnotations.Schema;");
                sb.AppendLine("");
                sb.AppendLine("namespace Sigma.DAL.Models");
                sb.AppendLine("{");
                sb.AppendLine("    [Serializable]");
                sb.AppendLine("    [Table(\"" + table.TableName + "\")] ");
                sb.AppendLine("    public class " + table.TableName + "Model");
                sb.AppendLine("    {");
                sb.AppendLine("        ////////////////////////////////////////////////////////////////////////////");
                sb.AppendLine("        // GENERATED CLASS - LAST GENERATED " + DateTime.Now.ToString());
                sb.AppendLine("        ////////////////////////////////////////////////////////////////////////////");
                sb.AppendLine("        [Key]");
                sb.AppendLine("        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]");
                sb.AppendLine($"        [Column(\"{ table.TableName.ToLower()  } Id\")]");
                sb.AppendLine("        public int " + table.PrimaryKey + " { get; set; }");
                sb.AppendLine("");
                foreach (var column in table.Columns.Where(x => !x.ColumnName.Equals(table.PrimaryKey)).OrderBy(x => x.ColumnId))
                {
                    sb.AppendLine($"        [Display(Name = \"{ column.Description  }\")]");
                    if (column.DataType == "nvarchar" || column.DataType == "varchar")
                    {
                         sb.AppendLine($"        [StringLength({ column.CharacterMaxLength}, ErrorMessage=\"The { column.Description } cannot exceed { column.CharacterMaxLength } characters.\")]");
                    }
                    if (column.DataType == "decimal")
                    {
                        sb.AppendLine("        [RegularExpression(@\"^\\d+.?\\d{0," + column.NumericScale + "}$\", ErrorMessage=\"The field '" + column.Description + "' can only have a maximum of " + column.NumericScale + " decimals.\")]");
                        sb.AppendLine("        [Range(0, " + new string('9', column.NumericPrecision -2) + ".99, ErrorMessage=\"The field '" + column.Description + "' can only have a maximum of " + column.NumericPrecision + " digits.\")]");
                    }
                    if (column.IsNullable == false)
                    {
                        sb.AppendLine("        [Required(ErrorMessage=\"The field '" + column.Description + "' cannot be empty.\")]");
                    }
                    sb.AppendLine("        public " + DataHelper.GetCSharpType(column.DataType) + " " + column.ColumnName + " { get; set; }");
                    sb.AppendLine(""); 
                }
                sb.AppendLine("    }");
                sb.AppendLine("}");
                sb.AppendLine("");
                File.WriteAllText($@"{ Program.outputDir }\Models\{ table.TableName }Model.cs", sb.ToString());
            }
        }
    }
}
