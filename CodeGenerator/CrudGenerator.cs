﻿using CodeGenerator.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static CodeGenerator.SqlInfo;

namespace CodeGenerator
{
    public class CrudGenerator
    {
        public void GenerateCrud(List<Table> tableList)
        {
            var sb = new StringBuilder();
            sb.AppendLine("using Dapper;");
            sb.AppendLine("using Microsoft.Data.SqlClient;");
            sb.AppendLine("using Sigma.DAL.Models;");
            sb.AppendLine("using System.Collections.Generic;");
            sb.AppendLine("using System.Data;");
            sb.AppendLine("using System.Threading.Tasks;");
            sb.AppendLine("using System.Linq;");
            sb.AppendLine("");
            sb.AppendLine("namespace Sigma.DAL.Repo");
            sb.AppendLine("{");
            sb.AppendLine("    ////////////////////////////////////////////////////////////////////////////");
            sb.AppendLine("    // GENERATED FILE - LAST GENERATED " + DateTime.Now.ToString());
            sb.AppendLine("    ////////////////////////////////////////////////////////////////////////////");
            foreach (var table in tableList)
            {
                sb.AppendLine("    public partial class " + table.TableName + "Repo");
                sb.AppendLine("    {");
                sb.AppendLine("        private async Task<" + table.TableName + "Model> GetByIdAsync(string tenantId, int " + table.TableName.ToLower() + "Id)");
                sb.AppendLine("        {");
                sb.AppendLine("            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;");
                sb.AppendLine("            var parameters = new { @" + table.PrimaryKey + " = " + table.TableName.ToLower() + "Id };");
                sb.AppendLine("            using (var conn = new SqlConnection(connstr))");
                sb.AppendLine("            {");
                sb.AppendLine("                return await Task.FromResult(conn.Query<" + table.TableName + "Model>(\"SELECT * FROM " + table.TableName +  " WHERE " + table.PrimaryKey + " = @" + table.PrimaryKey + ";\", parameters, commandTimeout: 5).FirstOrDefault());");
                sb.AppendLine("            } ");
                sb.AppendLine("        }");
                sb.AppendLine("");
                sb.AppendLine("        private async Task<long> AddAsync(string tenantId, " + table.TableName + "Model " + table.TableName.ToLower() + ")");
                sb.AppendLine("        {");
                sb.AppendLine("            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;");
                
                var columns = table.Columns.Where(x => !x.ColumnName.Equals(table.PrimaryKey)).OrderBy(x => x.ColumnId);
                var columnNames = columns.Select(x => x.ColumnName);

                sb.AppendLine("            var sql = \"INSERT INTO " + table.TableName + "(" + String.Join(",", columnNames) + ") \";");
                sb.AppendLine("                sql += \"VALUES(" + String.Join(",", columnNames.Select(x => "@" + x)) + ")\"; ");
                sb.AppendLine("            using (var conn = new SqlConnection(connstr))");
                sb.AppendLine("            {");
                sb.AppendLine("                conn.Open();");
                sb.AppendLine("                var parameters = new DynamicParameters();");
                foreach (var column in columns)
                {
                    if (column.DataType == "decimal")
                    {
                        sb.AppendLine("                parameters.Add(\"@" + column.ColumnName + "\", " + table.TableName.ToLower() + "." + column.ColumnName + ", DbType." + DataHelper.FirstCharToUpper(column.DataType) + ", ParameterDirection.Input, " + column.NumericPrecision + ", " + column.NumericScale +  ");");
                    }
                    else if (column.DataType == "int")
                    {
                        sb.AppendLine("                parameters.Add(\"@" + column.ColumnName + "\", " + table.TableName.ToLower() + "." + column.ColumnName + ", DbType.Int32, ParameterDirection.Input);");
                    }
                    else if (column.DataType == "nvarchar" || column.DataType == "varchar")
                    {
                        sb.AppendLine("                parameters.Add(\"@" + column.ColumnName + "\", " + table.TableName.ToLower() + "." + column.ColumnName + ", DbType.String"  + ", ParameterDirection.Input, " + table.TableName.ToLower() + "." + column.ColumnName + ".Length);");
                    }
                    else
                    {
                        sb.AppendLine("                parameters.Add(\"@" + column.ColumnName + "\", " + table.TableName.ToLower() + "." + column.ColumnName + ", DbType." + DataHelper.FirstCharToUpper(column.DataType) + ", ParameterDirection.Input);");
                    }
                }
                sb.AppendLine("                return await conn.ExecuteAsync(sql, parameters);");
                sb.AppendLine("            }");
                sb.AppendLine("        }");
                sb.AppendLine("");
                sb.AppendLine("        private async Task<long> UpdateAsync(string tenantId, " + table.TableName + "Model " + table.TableName.ToLower() + ")");
                sb.AppendLine("        {");
                sb.AppendLine("            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;");
                sb.AppendLine("            var sql = \"UPDATE " + table.TableName + " SET \";");
                var i = 1;
                foreach (var column in columns)
                {
                    if (i == columns.Count())
                    {
                        sb.AppendLine("            sql += \"" + column.ColumnName + " = @" + column.ColumnName + " \";");
                    }
                    else
                    {
                        sb.AppendLine("            sql += \"" + column.ColumnName + " = @" + column.ColumnName + ", \";");
                    }
                    i++;
                }
                sb.AppendLine("            sql += \"WHERE " + table.PrimaryKey + " = @" + table.PrimaryKey + "; \";");
                sb.AppendLine("            using (var conn = new SqlConnection(connstr))");
                sb.AppendLine("            {");
                sb.AppendLine("                conn.Open();");
                sb.AppendLine("                var parameters = new DynamicParameters();");
                foreach (var column in columns)
                {
                    if (column.DataType == "decimal")
                    {
                        sb.AppendLine("                parameters.Add(\"@" + column.ColumnName + "\", " + table.TableName.ToLower() + "." + column.ColumnName + ", DbType." + DataHelper.FirstCharToUpper(column.DataType) + ", ParameterDirection.Input, " + column.NumericPrecision + ", " + column.NumericScale + ");");
                    }
                    else if (column.DataType == "int")
                    {
                        sb.AppendLine("                parameters.Add(\"@" + column.ColumnName + "\", " + table.TableName.ToLower() + "." + column.ColumnName + ", DbType.Int32, ParameterDirection.Input);");
                    }
                    else if (column.DataType == "nvarchar" || column.DataType == "varchar")
                    {
                        sb.AppendLine("                parameters.Add(\"@" + column.ColumnName + "\", " + table.TableName.ToLower() + "." + column.ColumnName + ", DbType.String" + ", ParameterDirection.Input, " + table.TableName.ToLower() + "." + column.ColumnName + ".Length);");
                    }
                    else
                    {
                        sb.AppendLine("                parameters.Add(\"@" + column.ColumnName + "\", " + table.TableName.ToLower() + "." + column.ColumnName + ", DbType." + DataHelper.FirstCharToUpper(column.DataType) + ", ParameterDirection.Input);");
                    }
                }
                sb.AppendLine("                return await conn.ExecuteAsync(sql, parameters);");
                sb.AppendLine("            }");
                sb.AppendLine("        }");
                sb.AppendLine("    }    ");
                sb.AppendLine("");
            }
            sb.AppendLine("}   ");
            sb.AppendLine("");
            sb.AppendLine("");
            File.WriteAllText($@"{ Program.outputDir }\Crud\Crud.cs", sb.ToString());
        }
    }
}
