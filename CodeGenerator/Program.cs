﻿using System;
using System.Collections.Generic;

namespace CodeGenerator
{
    class Program
    {
        public static readonly string connstr = "Data Source=.;Initial Catalog=BigBankPlc;user id=sa;password=michelle; Integrated Security=False;";
        public static readonly string outputDir = @"C:\b";
      
        static void Main(string[] args)
        {
            var ignoreTablesList = new List<string>() { "UserInfo" };

            var gi = new SqlInfo();
            var tableList = gi.GetTableList(ignoreTablesList);

            var mg = new ModelGenerator();
            mg.GenerateModels(tableList);

            var cg = new CrudGenerator();
            cg.GenerateCrud(tableList);

            Console.WriteLine("Code generaion complete !");
        }
    }
}
