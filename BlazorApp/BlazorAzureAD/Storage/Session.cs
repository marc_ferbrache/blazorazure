﻿using System.Collections.Generic;

namespace Sigma.BlazorApp.Storage
{
    /// <summary>
    /// ///////////////////////////////////////////////////////////////////////////////////////
    /// This can be used to persist data between pages - as there are no session variables.
    /// In the Startup.cs file this class is injected as 'scoped' - meaning it will be 
    /// applicable (unique) to each client. This should be used sparingly, as SignalR 
    /// will store the variables in memory on the server for every user.
    /// ///////////////////////////////////////////////////////////////////////////////////////
    /// </summary>
    public interface ISession
    {
        dynamic GetItem(string key);
        void AddItem(string key, dynamic value);
        void RemoveItem(string key);
        Dictionary<string, dynamic> GetDictionary();
    }

    public class Session : ISession
    {
        private Dictionary<string, dynamic> DataStore { get; set;}

        public Session()
        {
            DataStore = new Dictionary<string, dynamic>();
        }

        public dynamic GetItem(string key)
        {
            key = key.ToUpper();
            if (DataStore.ContainsKey(key))
            {
                return DataStore[key];
            }
            else
            {
                return null;
            }
        }

        public void AddItem(string key, dynamic value)
        {
            key = key.ToUpper();
            if (DataStore.ContainsKey(key))
            {
                DataStore[key] = value;
            }
            else
            {
                DataStore.Add(key, value);
            }
        }

        public void RemoveItem(string key)
        {
            key = key.ToUpper();
            if (DataStore.ContainsKey(key))
            {
                DataStore.Remove(key.ToUpper());
            }
        }

        public Dictionary<string, dynamic> GetDictionary()
        {
            return DataStore;
        }
    }
}
