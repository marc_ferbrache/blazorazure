﻿using Sigma.DAL.Repo;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Identity.Web;
using System.Diagnostics;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace Sigma.BlazorApp.Security
{
    public class ClaimsTransformer : IClaimsTransformation
    {
        /// <summary>
        /// 1) Each time a user views a page this runs.  
        /// 2) Roles are obtained and assigned to the user.
        /// 3) As it runs on each request, code running within is kept to a minimum 
        /// </summary>

        readonly IUserInfoRepo _userInfoRepo;

        private bool isTransformed = false;

        public ClaimsTransformer(IUserInfoRepo userInfoRepo)
        {
            _userInfoRepo = userInfoRepo;
        }

        [DebuggerStepThrough]
        public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            if (!isTransformed)
            {
                var ci = (ClaimsIdentity)principal.Identity;

                var claims = principal.Identities.First().Claims.ToList();
                var username = claims?.FirstOrDefault(x => x.Type.Equals("preferred_username", StringComparison.OrdinalIgnoreCase))?.Value;
                var tenantId = principal.GetTenantId();

                // Query is cached in Redis in the data access layer
                var roleArr = _userInfoRepo.GetUserInfo(tenantId, username).Roles.Split(",");
                for (int i = 0; i < roleArr.Length; i++)
                {
                    // Assign roles
                    ci.AddClaim(new Claim(ci.RoleClaimType, roleArr[i]));
                }

                isTransformed = true;
            }
            return await Task.FromResult(principal);
        }

    }
}
