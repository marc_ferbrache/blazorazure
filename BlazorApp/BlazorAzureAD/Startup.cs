using System;
using Sigma.DAL.Repo;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureAD.UI;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PostSharp.Patterns.Caching;
using PostSharp.Patterns.Caching.Backends.Redis;
using StackExchange.Redis;
using System.Collections.Generic;
using Sigma.BlazorApp.Security;
using System.Globalization;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.Data.SqlClient.AlwaysEncrypted.AzureKeyVaultProvider;
using Microsoft.AspNetCore.Components.Server.Circuits;
using BlazorCircuitHandler.Services;
using Sigma.BlazorApp.Storage;

namespace Sigma.BlazorApp
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private ClientCredential _clientCredential;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTelerikBlazor();

            services.AddAuthentication(AzureADDefaults.AuthenticationScheme)
                    .AddAzureAD(options => Configuration.Bind("AzureAd", options));

            // Only allow permited tenants to use this app
            services.Configure<OpenIdConnectOptions>(AzureADDefaults.OpenIdScheme, options =>
            {
                options.Authority = options.Authority + "/v2.0/";
                options.TokenValidationParameters.ValidateIssuer = true;
                options.TokenValidationParameters.NameClaimType = "name";
                options.TokenValidationParameters.ValidIssuers = GetAllowedTenants();
            });

            services.AddControllersWithViews(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddRazorPages();
            services.AddServerSideBlazor();

            // CircuitHandler - want to somehow logout user if browser closed 
            services.AddSingleton<CircuitHandler>(new CircuitHandlerService());

            // Setup Dependency Injection for all data repository objects
            services.AddSingleton<IUserInfoRepo, UserInfoRepo>();
            services.AddSingleton<IPaymentRepo, PaymentRepo>();
            services.AddSingleton<IEmployeeRepo, EmployeeRepo>();
            services.AddSingleton<ICompanyRepo, CompanyRepo>();

            // Used to store cricial temporary session variables, use sparingly
            services.AddScoped<ISession, Session>();

            // Custom authorization (roles)
            services.AddScoped<IClaimsTransformation, ClaimsTransformer>();

            // Redis Server for data caching (postsharp) - Azure Redis
            string connectionConfiguration = "127.0.0.1";//"SigmaPayrollRedis.redis.cache.windows.net:6380,password=ak68LhMuhe5KQh9aRkZ0BPeTzBOG7pOvg1uyu1g8ook=,ssl=True,abortConnect=False"; // Dev = 127.0.0.1
            var connection = ConnectionMultiplexer.Connect(connectionConfiguration);
            var redisCachingConfiguration = new RedisCachingBackendConfiguration();
            CachingServices.DefaultBackend = RedisCachingBackend.Create(connection, redisCachingConfiguration);

#if DEBUG
            CachingServices.Profiles.Default.IsEnabled = false;
#else
            CachingServices.Profiles.Default.IsEnabled = true;
#endif

            // SQL Server AlwaysEncrypted key used by ADO.NET - Stored in Azure Vault 
            InitializeAzureKeyVaultProvider();
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                Microsoft.IdentityModel.Logging.IdentityModelEventSource.ShowPII = true;  // remove this later, security risk
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Load all tenants into a static list - call again if new tenant is added somwhere
            TenantRepo.LoadAllTenants(Configuration.GetSection("ConnectionStrings").GetSection("TenantDb").Get<string>());

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            // Need to look at this later, when we have time, in case we roll out to a US territory
            var culture = new CultureInfo("en-GB");
            CultureInfo.CurrentCulture = culture;
            CultureInfo.CurrentUICulture = culture;

            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }


        List<string> GetAllowedTenants()
        {
            // Format the full tenant issuer strings (from appsettings.json) for comparison
            var tenantList = Configuration.GetSection("Tenants").GetSection("TenantList").Get<List<string>>();
            var newTenantList = new List<string>();
            foreach(var tenant in tenantList)
            {
                newTenantList.Add($"https://login.microsoftonline.com/{ tenant }/v2.0");
            }
            return newTenantList;
        }

        void InitializeAzureKeyVaultProvider()
        {
            // Need to access this upon app initialization, by getting an encrypted item in the db. It is slow to start first time
            string applicationId = Configuration.GetSection("AzureAD").GetSection("ClientId").Get<string>();
            string clientKey = Configuration.GetSection("SigmaCustom").GetSection("AzureADAuthClientSecret").Get<string>();
            _clientCredential = new ClientCredential(applicationId, clientKey);

            SqlColumnEncryptionAzureKeyVaultProvider azureKeyVaultProvider =  new SqlColumnEncryptionAzureKeyVaultProvider(GetToken);
            var providers = new Dictionary<string, SqlColumnEncryptionKeyStoreProvider>();
            providers.Add(SqlColumnEncryptionAzureKeyVaultProvider.ProviderName, azureKeyVaultProvider);
            SqlConnection.RegisterColumnEncryptionKeyStoreProviders(providers);
        }

        async Task<string> GetToken(string authority, string resource, string scope)
        {
            var authContext = new AuthenticationContext(authority);
            AuthenticationResult result = await authContext.AcquireTokenAsync(resource, _clientCredential);
            if (result == null)
            {
                throw new InvalidOperationException("Failed to obtain the access token");
            }
            return result.AccessToken;
        }

    }
}
