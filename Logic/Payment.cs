﻿using Sigma.DAL.Models;
using Sigma.DAL.Repo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sigma.Logic
{
    public class PaymentService
    {
        /// <summary>
        /// 1) Any requests from the UI should pass through this Logic layer
        /// 2) The data repositories are injected into the class constructor
        /// 3) When creating unit tests, then mock objects can be injected - in replacement of the real repositories
        /// </summary>

        private readonly IPaymentRepo _paymentRepo;
        private readonly IEmployeeRepo _employeeRepo;

        public PaymentService(IPaymentRepo paymentRepo, IEmployeeRepo employeeRepo)
        {
            _paymentRepo = paymentRepo;
            _employeeRepo = employeeRepo;
        }
               
        public Task<IEnumerable<PaymentModel>> GetAllPaymentsAsync(string tenantId, int employeeId)
        {
            // Gets a list of all historical payments made to an employee
            return Task.FromResult(_paymentRepo.GetPaymentsByEmployeeId(tenantId, employeeId));
        }


        public PaySlip GeneratePayslip(string tenantId, int employeeId, int paymentId)
        { 
            // Generate a PDF payslip
            var payslip = new PaySlip();
            var payment = _paymentRepo.GetPaymentById(tenantId, employeeId, paymentId);
            var tenantName = DAL.Repo.TenantRepo.GetTenantInfo(tenantId).TenantName; 
            var employee = _employeeRepo.GetEmployee(tenantId, employeeId);
            var payslipGenerator = new PayslipGenerator();
            var payslipRequest = new PayslipGenerator.PaySlipRequest()
            {
                CompanyName = tenantName,
                EmployeeName = employee.FirstName + " " + employee.Surname,
                DatePaid = payment.DatePaid,
                PeriodStart = payment.PeriodStart,
                PeriodEnd = payment.PeriodEnd,
                Gross = payment.Gross,
                Ins = payment.Ins,
                Tax = payment.Tax,
                Paid = payment.Paid
            };
            payslip.FileContent = payslipGenerator.GeneratePayslip(payslipRequest);
            payslip.FileName = CreatePayslipFileName(employee.FirstName, employee.Surname, payment.DatePaid);
            return payslip;
        }

        private string CreatePayslipFileName(string firstName, string surname, DateTime date)
        {
            var sb = new StringBuilder();
            sb.Append(firstName + "_");
            sb.Append(surname + "_");
            sb.Append(date.Year.ToString() + "_");
            sb.Append(date.Month.ToString() + "_");
            sb.Append(date.Day.ToString() + ".pdf");
            return sb.ToString();
        }

        public class PaySlip
        {
            public string FileName { get; set; }
            public byte[] FileContent { get; set; }
        }
    }
}
