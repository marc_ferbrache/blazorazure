﻿using System;
using System.IO;
using Telerik.Documents.Primitives;
using Telerik.Windows.Documents.Fixed.FormatProviders.Pdf;
using Telerik.Windows.Documents.Fixed.FormatProviders.Pdf.Export;
using Telerik.Windows.Documents.Fixed.Model;
using Telerik.Windows.Documents.Fixed.Model.ColorSpaces;
using Telerik.Windows.Documents.Fixed.Model.Editing;
using Telerik.Windows.Documents.Fixed.Model.Editing.Flow;
using Telerik.Windows.Documents.Fixed.Model.Editing.Tables;
using Telerik.Windows.Documents.Fixed.Model.Fonts;

namespace Sigma.Logic
{
    public class PayslipGenerator
    {
        /// <summary>
        /// This has been used to create a pdf payslip file. It is temporary for demo purposes and will be deleted 
        /// once a reporting solution has been found.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public byte[] GeneratePayslip(PaySlipRequest request)
        {
            PdfFormatProvider formatProvider = new PdfFormatProvider();
            formatProvider.ExportSettings.ImageQuality = ImageQuality.High;

            byte[] renderedBytes = null;
            using (MemoryStream ms = new MemoryStream())
            {
                RadFixedDocument document = CreateDocument(request);
                formatProvider.Export(document, ms);
                renderedBytes = ms.ToArray();
            }
            return renderedBytes;
        }

        private RadFixedDocument CreateDocument(PaySlipRequest request)
        {
            double defaultLeftIndent = 50;
            double defaultLineHeight = 18;
            double currentTopOffset = 40;

            RadFixedDocument document = new RadFixedDocument();
            RadFixedPage page = document.Pages.AddPage();
            page.Size = new Size(600, 750);
            FixedContentEditor editor = new FixedContentEditor(page);
            editor.Position.Translate(defaultLeftIndent, 50);

            editor.Position.Translate(defaultLeftIndent, currentTopOffset);
            editor.TextProperties.FontSize = 14;

            double maxWidth = page.Size.Width - defaultLeftIndent * 2;

            Block block = new Block();
            block.GraphicProperties.FillColor = RgbColors.Black;
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Left;
            block.TextProperties.Font = FontsRepository.HelveticaBold;
            block.TextProperties.FontSize = 18;
            block.InsertText(request.CompanyName);
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            block = new Block();
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Right;
            block.TextProperties.Font = FontsRepository.HelveticaBold;
            block.InsertText(request.DatePaid.ToShortDateString());
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            currentTopOffset += defaultLineHeight * 2;
            editor.Position.Translate(defaultLeftIndent, currentTopOffset);

            block = new Block();
            block.GraphicProperties.FillColor = RgbColors.Black;
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Left;
            block.TextProperties.Font = FontsRepository.HelveticaBold;
            block.TextProperties.FontSize = 14;
            block.InsertText($"Pay Advice For:   { request.EmployeeName }");
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            block = new Block();
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Right;
            block.TextProperties.Font = FontsRepository.HelveticaBold;
            block.InsertText($"Period: { request.PeriodStart.ToShortDateString() } - { request.PeriodEnd.ToShortDateString() }");
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            currentTopOffset += defaultLineHeight * 4;
            editor.Position.Translate(defaultLeftIndent, currentTopOffset);

            block = new Block();
            block.GraphicProperties.FillColor = RgbColors.Black;
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Center;
            block.TextProperties.Font = FontsRepository.HelveticaBold;
            block.InsertText(request.CompanyName);
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            currentTopOffset += defaultLineHeight;
            editor.Position.Translate(defaultLeftIndent, currentTopOffset);

            block = new Block();
            block.GraphicProperties.FillColor = RgbColors.Black;
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Center;
            block.TextProperties.Font = FontsRepository.HelveticaBold;
            block.TextProperties.FontSize = 24;
            block.InsertText("Pay Advice");
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            currentTopOffset += defaultLineHeight * 2;
            editor.Position.Translate(defaultLeftIndent, currentTopOffset);

            block = new Block();
            block.GraphicProperties.FillColor = RgbColors.Black;
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Center;
            block.TextProperties.Font = FontsRepository.Helvetica;
            block.TextProperties.FontSize = 11;
            block.InsertText("for");
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            currentTopOffset += defaultLineHeight;
            editor.Position.Translate(defaultLeftIndent, currentTopOffset);

            block = new Block();
            block.GraphicProperties.FillColor = RgbColors.Black;
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Center;
            block.TextProperties.Font = FontsRepository.HelveticaBold;
            block.InsertText(request.EmployeeName);
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            currentTopOffset += defaultLineHeight * 2;
            editor.Position.Translate(defaultLeftIndent, currentTopOffset);

            block = new Block();
            block.GraphicProperties.FillColor = RgbColors.Black;
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Left;
            block.TextProperties.Font = FontsRepository.Helvetica;
            block.InsertText($"   Date: { request.DatePaid.ToShortDateString() }");
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            block = new Block();
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Right;
            block.TextProperties.Font = FontsRepository.Helvetica;
            block.InsertText($"Gross Pay:   £ { request.Gross.ToString("N2") }");
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            currentTopOffset += defaultLineHeight;
            editor.Position.Translate(defaultLeftIndent, currentTopOffset);

            block = new Block();
            block.GraphicProperties.FillColor = RgbColors.Black;
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Left;
            block.TextProperties.Font = FontsRepository.Helvetica;
            block.InsertText($"Period: { request.PeriodStart.ToShortDateString() } - { request.PeriodEnd.ToShortDateString() }");
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            block = new Block();
            block.HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment.Right;
            block.TextProperties.Font = FontsRepository.HelveticaBold;
            block.InsertText($"Net Pay:   £ { request.Paid.ToString("N2") }");
            editor.DrawBlock(block, new Size(maxWidth, double.PositiveInfinity));

            currentTopOffset += defaultLineHeight * 2;
            editor.Position.Translate(defaultLeftIndent, currentTopOffset);

            Table table = new Table();
            Border border = new Border();
            table.DefaultCellProperties.Borders = new TableCellBorders(border, border, border, border);
            table.DefaultCellProperties.Padding = new Thickness(10);
            table.LayoutType = TableLayoutType.FixedWidth;
            TableRow firstRow = table.Rows.AddTableRow();
            firstRow.Cells.AddTableCell().Blocks.AddBlock().InsertText("Base Salary");
            firstRow.Cells.AddTableCell().Blocks.AddBlock().InsertText($"{ request.Gross.ToString("N2") }");

            TableRow secondRow = table.Rows.AddTableRow();
            secondRow.Cells.AddTableCell().Blocks.AddBlock().InsertText("PAYG Tax Deduction");
            secondRow.Cells.AddTableCell().Blocks.AddBlock().InsertText($"{ request.Tax.ToString("N2") }");
            TableRow thirdRow = table.Rows.AddTableRow();
            thirdRow.Cells.AddTableCell().Blocks.AddBlock().InsertText("SS Deduction");
            thirdRow.Cells.AddTableCell().Blocks.AddBlock().InsertText($"{ request.Ins.ToString("N2") }");

            editor.DrawTable(table, new Size(500, 400));

            return document;
        }

        public class PaySlipRequest
        {
            public string CompanyName { get; set; }
            public string EmployeeName { get; set; }
            public DateTime DatePaid { get; set; }
            public DateTime PeriodStart { get; set; }
            public DateTime PeriodEnd { get; set; }
            public decimal Gross { get; set; }
            public decimal Tax { get; set; }
            public decimal Ins { get; set; }
            public decimal Paid { get; set; }

        }
    }
}

