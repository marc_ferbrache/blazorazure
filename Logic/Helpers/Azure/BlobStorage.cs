﻿using System;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Sigma.Logic.Helpers.Azure
{
    public class BlobStorage
    {
        private readonly string _azureBlobConnectionString;
        public BlobStorage(string azureBlobConnectionString)
        {
            // TODO: Transfer Azure Blob connection string to environment variables (more secure)  
            _azureBlobConnectionString = azureBlobConnectionString;
        }

        private CloudBlobClient GetCloudBlobClient()
        {
            CloudStorageAccount account;
            if (!CloudStorageAccount.TryParse(_azureBlobConnectionString, out account))
            {
                throw new Exception("Could not create blob client.");
            }
            return account.CreateCloudBlobClient();
        }

        public async Task<string> UploadFileAsync(string tenantId, string folder, string fileName, byte[] fileContent)
        {
            var blobClient = GetCloudBlobClient();
            // Create tenant container if not existing
            CloudBlobContainer container = blobClient.GetContainerReference($"{tenantId}/");
            await container.CreateIfNotExistsAsync();
            await container.SetPermissionsAsync(new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Container
            });
            // put file in directory of the container (directories are an abstraction)
            CloudBlockBlob blob = container.GetBlockBlobReference($"{folder}/{fileName}"); 
            await blob.UploadFromByteArrayAsync(fileContent, 0, fileContent.Length);
            return $"{blobClient.BaseUri}{folder}/{fileName}"; 
        }

        public async Task DeleteFileAsync(string tenantId, string folder, string fileName)
        {
            var blobClient = GetCloudBlobClient();
            var container = blobClient.GetContainerReference(tenantId);
            var blob = container.GetBlockBlobReference($"{folder}/{fileName}");
            await blob.DeleteIfExistsAsync();
        }

        public async Task<byte[]> DownloadFileAsync(string tenantId, string folder, string fileName)
        {
            var blobClient = GetCloudBlobClient();
            // Create container if not existing
            CloudBlobContainer container = blobClient.GetContainerReference(tenantId);
            container.SetPermissions(new BlobContainerPermissions 
            {  
                PublicAccess = BlobContainerPublicAccessType.Container 
            });
            await container.CreateIfNotExistsAsync();
            var blob = container.GetBlockBlobReference($"{folder}/{fileName}");
            using (var ms = new MemoryStream())
            {
                await blob.DownloadToStreamAsync(ms, new CancellationToken());
                return ms.ToArray();
            }
        }

        public async Task<List<string>> GetFileListInFolderAsync(string tenantId, string folder)
        {
            var fileList = new List<string>();
            var blobClient = GetCloudBlobClient();
            var container = blobClient.GetContainerReference(tenantId);
            await container.CreateIfNotExistsAsync();
            var directory = container.GetDirectoryReference(folder);
            var blobs = await directory.ListBlobsSegmentedAsync(true, BlobListingDetails.None, 500, null, null, null);
            foreach (CloudBlockBlob blob in blobs.Results)
            {
                var urlSegment = blob.Name.Split(@"/");
                Array.Reverse(urlSegment);
                fileList.Add(urlSegment[0]);
            }
            return fileList;
        }
    }
}
