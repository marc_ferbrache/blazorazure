﻿using Sigma.DAL.Models;
using Sigma.DAL.Repo;
using Sigma.Logic.Helpers.Azure;
using System.Threading.Tasks;

namespace Sigma.Logic
{
    public class CompanyInformation
    {
        private readonly string _azureBlobConnectionString;
        private readonly ICompanyRepo _companyRepo;

        public CompanyInformation(string azureBlobConnectionString, ICompanyRepo companyRepo)
        {
            _azureBlobConnectionString = azureBlobConnectionString;
            _companyRepo = companyRepo;
        }

        public async Task UploadCompanyLogoAsync(string tenantId, string fileName, byte[] fileContent)
        {
            var blobStorage = new BlobStorage(_azureBlobConnectionString);
            await blobStorage.DeleteFileAsync(tenantId, "logo", fileName);
            var url = await blobStorage.UploadFileAsync(tenantId, "logo", fileName, fileContent);
            _companyRepo.UpdateCompanyLogoUrl(tenantId, url);
        }

        public Task<CompanyModel> GetCompanyAsync(string tenantId)
        {
            return Task.FromResult(_companyRepo.GetCompany(tenantId));
        }
    }
}
