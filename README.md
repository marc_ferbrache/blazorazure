# README #

### What is this repository for? ###

* This is a project that I have used to demonstrate Blazor & Azure AD integration to a potential client.
* Now it is being used to support a job application at your company !

### How do I get set up? ###

* Don't bother trying to setup or compile the projects, just review the code.
* The Azure backend setup had been removed from Azure now (As I did not continue to pay them). 
* The code is provided only to demonstrate that I have an understanding of how Blazor works.
* In addition the code is meant to demonstrate that I have neat and organised approach (you decide).
* No tests are included, although the IoC is in place for them!

### Who do I talk to? ###

* Email me or I can send you Teams / Skype username
* Marc Ferbrache - marc@ferbrache.com

### Explain the projects? ###
* The 'BlazorApp' folder contains a Blazor server application (it also contains the main VS solution file).
* The 'Logic' folder is the source code for a middle tier DLL, containing any business logic.
* The 'DAL' folder is the source code for a DLL containing the 'data access layer' (DAL), this uses the 'Dapper Framework'. 
* The 'CodeGenerator' folder is a console app that creates CRUD & DTO (data transfer objects), contained within the DAL layer (previously mentioned).

### Explain the application? ###
* The app is a multi-tenant program allowing employees of a company(s) to login and view previous payslips.
* Users are authenticated/authorized using Azure Active Directory. The users/passwords are synced (with Azure AD) for each tenant's local domain controller.
* Data queries are cached within the DAL layer to a Redis cache (in mermory database), using a caching component from 'PostSharp'.
* After authentication, users may select and view a previous payslip.  These are then displayed within a Telerik UI grid.
* Payslip information is encrypted at rest, using native SQL Server encryption. The encryption keys are stored within Azure Vault.


### Extra Special Mentions! ###
* I have not touched this code for about 12 months.
* This is a prototype proof of concept system, it is not production ready.
* There are likely improvements or changes that could now be added, due to Blazor/Core changes. 

