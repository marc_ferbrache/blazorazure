using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sigma.DAL.Models
{
    [Serializable]
    [Table("Employee")] 
    public class EmployeeModel
    {
        ////////////////////////////////////////////////////////////////////////////
        // GENERATED CLASS - LAST GENERATED 20/06/2020 2:14:41 PM
        ////////////////////////////////////////////////////////////////////////////
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("employee Id")]
        public int EmployeeId { get; set; }

        [Display(Name = "first name")]
        [StringLength(50, ErrorMessage="The first name cannot exceed 50 characters.")]
        public string FirstName { get; set; }

        [Display(Name = "surname")]
        [StringLength(50, ErrorMessage="The surname cannot exceed 50 characters.")]
        public string Surname { get; set; }

    }
}

