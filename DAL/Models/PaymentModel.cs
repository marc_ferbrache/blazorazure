using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sigma.DAL.Models
{
    [Serializable]
    [Table("Payment")] 
    public class PaymentModel
    {
        ////////////////////////////////////////////////////////////////////////////
        // GENERATED CLASS - LAST GENERATED 20/06/2020 2:14:41 PM
        ////////////////////////////////////////////////////////////////////////////
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("payment Id")]
        public int PaymentId { get; set; }

        [Display(Name = "employee id")]
        [Required(ErrorMessage="The field 'employee id' cannot be empty.")]
        public int EmployeeId { get; set; }

        [Display(Name = "date paid")]
        public DateTime DatePaid { get; set; }

        [Display(Name = "period start")]
        public DateTime PeriodStart { get; set; }

        [Display(Name = "period end")]
        public DateTime PeriodEnd { get; set; }

        [Display(Name = "gross")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage="The field 'gross' can only have a maximum of 2 decimals.")]
        [Range(0, 9999999999999999.99, ErrorMessage="The field 'gross' can only have a maximum of 18 digits.")]
        public decimal Gross { get; set; }

        [Display(Name = "tax")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage="The field 'tax' can only have a maximum of 2 decimals.")]
        [Range(0, 9999999999999999.99, ErrorMessage="The field 'tax' can only have a maximum of 18 digits.")]
        public decimal Tax { get; set; }

        [Display(Name = "social insurance")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage="The field 'social insurance' can only have a maximum of 2 decimals.")]
        [Range(0, 9999999999999999.99, ErrorMessage="The field 'social insurance' can only have a maximum of 18 digits.")]
        public decimal Ins { get; set; }

        [Display(Name = "paid")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage="The field 'paid' can only have a maximum of 2 decimals.")]
        [Range(0, 9999999999999999.99, ErrorMessage="The field 'paid' can only have a maximum of 18 digits.")]
        public decimal Paid { get; set; }

    }
}

