﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Sigma.DAL.Repo
{
    public static class TenantRepo
    {
        /// <summary>
        /// 1) Used in securing the application. 
        /// 2) Loads all tenants into static list when program first starts.
        /// 3) Tenant info can be obtained from 'static list' by tenantId.
        /// 4) Used in obtaining the tenant connection string.
        /// 5) Contains its own nested model, as crud generation not needed.
        /// 6) Static class, not injected, so no interface needed.
        /// </summary>

        public static List<TenantModel> TenantList = new List<TenantModel>();

        public static void LoadAllTenants(string tenantDbConnStr)
        {
            using (var conn = new SqlConnection(tenantDbConnStr))
            {
                TenantList = conn.Query<TenantModel>("SELECT * FROM Tenant;").ToList();
            }
        }

        public static TenantModel GetTenantInfo(string tenantId)
        {
            var result = TenantList.Where(x => x.TenantId.Equals(tenantId));
            if (!result.Any())
            {
                throw new System.Exception("Invalid or no tenant information found.");
            }
            return result.FirstOrDefault();
        }

        public class TenantModel
        {
            public string TenantId { get; set; }
            public string TenantName { get; set; }
            public string TenantConnStr { get; set; }
        }
    }
}
