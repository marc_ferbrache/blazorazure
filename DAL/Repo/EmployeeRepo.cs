﻿using Dapper;
using Microsoft.Data.SqlClient;
using PostSharp.Patterns.Caching;
using Sigma.DAL.Models;
using System.Linq;

namespace Sigma.DAL.Repo
{
    public interface IEmployeeRepo
    {
        EmployeeModel GetEmployee(string tenantId, int employeeId);
    }
    public partial class EmployeeRepo : IEmployeeRepo
    {
        [Cache(AbsoluteExpiration = 30)]
        public EmployeeModel GetEmployee(string tenantId, int employeeId)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var parameters = new { @EmployeeId = employeeId };
            using (var conn = new SqlConnection(connstr))
            {
                return conn.Query<EmployeeModel>("SELECT * FROM Employee WHERE EmployeeId = @EmployeeId;", parameters, commandTimeout: 5).FirstOrDefault();
            }
        }
    }
}
