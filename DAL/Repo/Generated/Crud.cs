using Dapper;
using Microsoft.Data.SqlClient;
using Sigma.DAL.Models;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Linq;

namespace Sigma.DAL.Repo
{
    ////////////////////////////////////////////////////////////////////////////
    // GENERATED FILE - LAST GENERATED 20/06/2020 4:31:28 PM
    ////////////////////////////////////////////////////////////////////////////
    public partial class EmployeeRepo
    {
        private async Task<EmployeeModel> GetByIdAsync(string tenantId, int employeeId)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var parameters = new { @EmployeeId = employeeId };
            using (var conn = new SqlConnection(connstr))
            {
                return await Task.FromResult(conn.Query<EmployeeModel>("SELECT * FROM Employee WHERE EmployeeId = @EmployeeId;", parameters, commandTimeout: 5).FirstOrDefault());
            } 
        }

        private async Task<long> AddAsync(string tenantId, EmployeeModel employee)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var sql = "INSERT INTO Employee(FirstName,Surname) ";
                sql += "VALUES(@FirstName,@Surname)"; 
            using (var conn = new SqlConnection(connstr))
            {
                conn.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@FirstName", employee.FirstName, DbType.String, ParameterDirection.Input, employee.FirstName.Length);
                parameters.Add("@Surname", employee.Surname, DbType.String, ParameterDirection.Input, employee.Surname.Length);
                return await conn.ExecuteAsync(sql, parameters);
            }
        }

        private async Task<long> UpdateAsync(string tenantId, EmployeeModel employee)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var sql = "UPDATE Employee SET ";
            sql += "FirstName = @FirstName, ";
            sql += "Surname = @Surname ";
            sql += "WHERE EmployeeId = @EmployeeId; ";
            using (var conn = new SqlConnection(connstr))
            {
                conn.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@FirstName", employee.FirstName, DbType.String, ParameterDirection.Input, employee.FirstName.Length);
                parameters.Add("@Surname", employee.Surname, DbType.String, ParameterDirection.Input, employee.Surname.Length);
                return await conn.ExecuteAsync(sql, parameters);
            }
        }
    }    

    public partial class PaymentRepo
    {
        private async Task<PaymentModel> GetByIdAsync(string tenantId, int paymentId)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var parameters = new { @PaymentId = paymentId };
            using (var conn = new SqlConnection(connstr))
            {
                return await Task.FromResult(conn.Query<PaymentModel>("SELECT * FROM Payment WHERE PaymentId = @PaymentId;", parameters, commandTimeout: 5).FirstOrDefault());
            } 
        }

        private async Task<long> AddAsync(string tenantId, PaymentModel payment)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var sql = "INSERT INTO Payment(EmployeeId,DatePaid,PeriodStart,PeriodEnd,Gross,Tax,Ins,Paid) ";
                sql += "VALUES(@EmployeeId,@DatePaid,@PeriodStart,@PeriodEnd,@Gross,@Tax,@Ins,@Paid)"; 
            using (var conn = new SqlConnection(connstr))
            {
                conn.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@EmployeeId", payment.EmployeeId, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@DatePaid", payment.DatePaid, DbType.Date, ParameterDirection.Input);
                parameters.Add("@PeriodStart", payment.PeriodStart, DbType.Date, ParameterDirection.Input);
                parameters.Add("@PeriodEnd", payment.PeriodEnd, DbType.Date, ParameterDirection.Input);
                parameters.Add("@Gross", payment.Gross, DbType.Decimal, ParameterDirection.Input, 18, 2);
                parameters.Add("@Tax", payment.Tax, DbType.Decimal, ParameterDirection.Input, 18, 2);
                parameters.Add("@Ins", payment.Ins, DbType.Decimal, ParameterDirection.Input, 18, 2);
                parameters.Add("@Paid", payment.Paid, DbType.Decimal, ParameterDirection.Input, 18, 2);
                return await conn.ExecuteAsync(sql, parameters);
            }
        }

        private async Task<long> UpdateAsync(string tenantId, PaymentModel payment)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var sql = "UPDATE Payment SET ";
            sql += "EmployeeId = @EmployeeId, ";
            sql += "DatePaid = @DatePaid, ";
            sql += "PeriodStart = @PeriodStart, ";
            sql += "PeriodEnd = @PeriodEnd, ";
            sql += "Gross = @Gross, ";
            sql += "Tax = @Tax, ";
            sql += "Ins = @Ins, ";
            sql += "Paid = @Paid ";
            sql += "WHERE PaymentId = @PaymentId; ";
            using (var conn = new SqlConnection(connstr))
            {
                conn.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@EmployeeId", payment.EmployeeId, DbType.Int32, ParameterDirection.Input);
                parameters.Add("@DatePaid", payment.DatePaid, DbType.Date, ParameterDirection.Input);
                parameters.Add("@PeriodStart", payment.PeriodStart, DbType.Date, ParameterDirection.Input);
                parameters.Add("@PeriodEnd", payment.PeriodEnd, DbType.Date, ParameterDirection.Input);
                parameters.Add("@Gross", payment.Gross, DbType.Decimal, ParameterDirection.Input, 18, 2);
                parameters.Add("@Tax", payment.Tax, DbType.Decimal, ParameterDirection.Input, 18, 2);
                parameters.Add("@Ins", payment.Ins, DbType.Decimal, ParameterDirection.Input, 18, 2);
                parameters.Add("@Paid", payment.Paid, DbType.Decimal, ParameterDirection.Input, 18, 2);
                return await conn.ExecuteAsync(sql, parameters);
            }
        }
    }    

    public partial class CompanyRepo
    {
        private async Task<CompanyModel> GetByIdAsync(string tenantId, int companyId)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var parameters = new { @CompanyId = companyId };
            using (var conn = new SqlConnection(connstr))
            {
                return await Task.FromResult(conn.Query<CompanyModel>("SELECT * FROM Company WHERE CompanyId = @CompanyId;", parameters, commandTimeout: 5).FirstOrDefault());
            } 
        }

        private async Task<long> AddAsync(string tenantId, CompanyModel company)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var sql = "INSERT INTO Company(CompanyName,CompanyLogo) ";
                sql += "VALUES(@CompanyName,@CompanyLogo)"; 
            using (var conn = new SqlConnection(connstr))
            {
                conn.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@CompanyName", company.CompanyName, DbType.String, ParameterDirection.Input, company.CompanyName.Length);
                parameters.Add("@CompanyLogo", company.CompanyLogo, DbType.String, ParameterDirection.Input, company.CompanyLogo.Length);
                return await conn.ExecuteAsync(sql, parameters);
            }
        }

        private async Task<long> UpdateAsync(string tenantId, CompanyModel company)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var sql = "UPDATE Company SET ";
            sql += "CompanyName = @CompanyName, ";
            sql += "CompanyLogo = @CompanyLogo ";
            sql += "WHERE CompanyId = @CompanyId; ";
            using (var conn = new SqlConnection(connstr))
            {
                conn.Open();
                var parameters = new DynamicParameters();
                parameters.Add("@CompanyName", company.CompanyName, DbType.String, ParameterDirection.Input, company.CompanyName.Length);
                parameters.Add("@CompanyLogo", company.CompanyLogo, DbType.String, ParameterDirection.Input, company.CompanyLogo.Length);
                return await conn.ExecuteAsync(sql, parameters);
            }
        }
    }    

}   


