﻿using Dapper;
using PostSharp.Patterns.Caching;
using Sigma.DAL.Models;
using Microsoft.Data.SqlClient;
using System.Linq;

namespace Sigma.DAL.Repo
{
    public interface ICompanyRepo
    {
        long UpdateCompanyLogoUrl(string tenantId, string companyLogo);
        CompanyModel GetCompany(string tenantId);
    }
    public partial class CompanyRepo : ICompanyRepo
    {
        [InvalidateCache(nameof(GetCompany))]
        public long UpdateCompanyLogoUrl(string tenantId, string companyLogo)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var parameters = new { @CompanyLogo = companyLogo };
            using (var conn = new SqlConnection(connstr))
            {
                return conn.Execute("UPDATE Company SET CompanyLogo = @CompanyLogo;", parameters, commandTimeout: 5);
            }
        }

        [Cache(AbsoluteExpiration = 30)]
        public CompanyModel GetCompany(string tenantId)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            using (var conn = new SqlConnection(connstr))
            {
                return conn.Query<CompanyModel>("SELECT * FROM Company;").FirstOrDefault();
            }
        }
    }
}
