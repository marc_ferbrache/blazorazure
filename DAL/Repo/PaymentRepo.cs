﻿using Dapper;
using Microsoft.Data.SqlClient;
using PostSharp.Patterns.Caching;
using Sigma.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sigma.DAL.Repo
{
    public interface IPaymentRepo
    {
        Task<long> AddPaymentAsync(string tenantId, int employeeId, PaymentModel payment);
        IEnumerable<PaymentModel> GetPaymentsByEmployeeId(string tenantId, int employeeId);
        PaymentModel GetPaymentById(string tenantId, int employeeId, int paymentId);
    }

    public partial class PaymentRepo : IPaymentRepo
    {
        [InvalidateCache(nameof(GetPaymentsByEmployeeId))]
        public async Task<long> AddPaymentAsync(string tenantId, int employeeId, PaymentModel payment)
        {
            // Note: TenantId and employeeId are both needed in the method signature [of method 'Add()'], to allow cache Invalidation.
            // This is a passthrough to the generated crud private methods
            // If allows control of the cache invalidation, which has to be included manually (as there are too many options)
            return await AddAsync(tenantId, payment);            
        }

        [Cache(AbsoluteExpiration = 30)]
        public IEnumerable<PaymentModel> GetPaymentsByEmployeeId(string tenantId, int employeeId)
        {
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var parameters = new { @EmployeeId = employeeId };
            using (var conn = new SqlConnection(connstr))
            {
                return conn.Query<PaymentModel>("SELECT * FROM Payment WHERE EmployeeId = @EmployeeId ORDER BY DatePaid DESC;", parameters, commandTimeout: 5);
            }
        }

        [Cache(AbsoluteExpiration = 30)]
        public PaymentModel GetPaymentById(string tenantId, int employeeId, int paymentId)
        {
            return GetByIdAsync(tenantId, paymentId).Result;
        }

    }
}
