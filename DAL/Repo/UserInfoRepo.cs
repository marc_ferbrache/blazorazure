﻿using Dapper;
using PostSharp.Patterns.Caching;
using System;
using Microsoft.Data.SqlClient;
using System.Linq;
using static Sigma.DAL.Repo.UserInfoRepo;

namespace Sigma.DAL.Repo
{
    public interface IUserInfoRepo
    {
        UserInfoModel GetUserInfo(string tenantId, string userName);
        void UpdateUserInfo(string tenantId, string userName, string roles);
    }

    public class UserInfoRepo : IUserInfoRepo
    {
        /// <summary>
        /// 1) Used in securing the application
        /// 2) Class contains roles & Id applicable to the user... in the users tenant database.
        /// 3) Data is cached in Redis, it can be invalidated  
        /// 4) Contains its own nested model, as crud generation is not needed.
        /// </summary>

        [Cache(AbsoluteExpiration = 30)] // 30 minutes
        public UserInfoModel GetUserInfo(string tenantId, string userName)
        {
            var retVal = new UserInfoModel();

            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            var parameters = new { UserName = userName };
            using (var conn = new SqlConnection(connstr))
            {
                retVal =  conn.Query<UserInfoModel>("SELECT * FROM UserInfo WHERE UserName = @UserName", parameters, commandTimeout: 5).First();
            }
            return retVal;
        }

        [InvalidateCache(nameof(GetUserInfo))]
        public void UpdateUserInfo(string tenantId, string userName, string roles)
        {
            // Update user roles
            var connstr = TenantRepo.GetTenantInfo(tenantId).TenantConnStr;
            // TODO:
            // 1 - Clear redis for GetRoles() method
            // 2 - Update roles in DB & redis 
            // 3 - Call GetUserInfo() again to cache it
            var userInfo = GetUserInfo(tenantId, userName);
        }

        [Serializable]
        public class UserInfoModel
        {
            public int Id { get; set; }
            public int EmployeeId { get; set; }
            public string Roles { get; set; }
        }
    }
}
